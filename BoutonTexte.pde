class BoutonTexte extends Bouton {
  String nb = "1";
  int val = 0;
  boolean bascule = false;

  BoutonTexte(float _x, float _y, float _w, String _t) {
    super( _x, _y, _w, _t);
  }

  void miseAjour() {
    super.miseAjour();
    // on ajoute à la méthode miseAjour de la classe Bouton:
    if (active) {
      bascule = !bascule;
      nb = "";
    }

    if (bascule) {
      if (keyPressed) {
        if (key >='1' && key <='9') {
          nb = str(key);
        }
        if (key == ENTER) {
           val = int(nb);
           bascule = false;
        }
      }
    }
  }

  void dessine() {
    super.dessine();

    if (bascule) {
      if (second() % 2 == 0) {
        stroke(255);
      }
      else {
        stroke(0);
      }

      line(x-w/3, (y-w/2)+5, x-w/3, (y+w/2)-5);
    }
    textAlign(CENTER, CENTER);
    fill(255);
    text(nb, x, y);
  }
}

