import seltar.unzipit.*;
import javax.swing.*; 
import java.io.File;
import java.util.Iterator;

UnZipIt zip;
String monChemin;
XML content;
Bouton selection;
BoutonTexte tailleMots;
BoutonTexte nbOccurences;
ArrayList tousLesMots;
HashMap dico;
PFont f;
int nbTotaldeMots = 0;
boolean unzipFini = false;
boolean analyseXMLFinie = false;
float W, H;

/*
×××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××
 */
void setup() {
  W = displayWidth;
  H = displayHeight;
  size((int)W, (int)H);
  background(255);
  fill(0);
  rectMode(CENTER);
  textSize(W/80);

  tousLesMots = new ArrayList();
  selection = new Bouton(W/4, H/15, 20, "Selectionnez un nouveau document");
  tailleMots = new BoutonTexte((W/2 + W/4), H/15, 20, "taille des mots recherchés");
  nbOccurences = new BoutonTexte((W/2 + W/4), H/10, 20, "nombre d'occurences");
  String[] fontList = PFont.list();
  println(fontList);
  f = createFont(fontList[250], 36, true);
}
/*
×××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××
 */
void draw() {
  background(#CFA0E9);
  fill(#B0F2B6);
  noStroke();
  rect(W/2, H/16, W, H/8);

  selection.miseAjour();
  tailleMots.miseAjour();
  nbOccurences.miseAjour();

  selection.dessine();
  tailleMots.dessine();
  nbOccurences.dessine();
  dessineInterface();

  // tant que l'analyse du fichier XML n'est pas terminée on va jusqu'au bout de l'opération
  // après on arrête
  if (selection.active) {
    tousLesMots.clear();
    nouveauDocument();
    if (unzipFini) {
      analyseXML();
      println("il y a "+ nbTotaldeMots+ " mots dans ce document");
    }
  }

  if (analyseXMLFinie) {
    compareMots();
  }
}
/*
×××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××
 */
void dessineInterface() {
  stroke(255);
  strokeWeight(3);
  line(0, H/8, W, H/8);
}
/*
×××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××
 */
void  nouveauDocument() {
  if (selection.active) 
    ouvertureFichier();
  selection.active = false; // permet d'éviter de lancer une quantité infini de fenêtres
}
/*
×××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××
 Ouverture de la fenetre JFileChooser pour selectionner un fichier
 */

void ouvertureFichier() {

  // --- nécessaire pour lancer la fenetre de navigation
  try { 
    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
  } 
  catch (Exception e) { 
    e.printStackTrace();
  }

  //  --- la fenetre de navigation
  final JFileChooser fc = new JFileChooser(); 
  int returnVal = fc.showOpenDialog(this);

  if (returnVal == JFileChooser.APPROVE_OPTION) {
    File file = fc.getSelectedFile();

    if (file.getName().endsWith(".odt")) {
      if (file != null) {
        monChemin = file.getAbsolutePath();
      }
    }
  }
  unzipFichier();
}

/*
×××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××
 on dézippe le fichier .odt qui est un .zip masqué
 */
void unzipFichier() {

  if (monChemin != null) {
    println(monChemin);
    // --- je dézippe le fichier .odt qui est un .zip masqué
    // --- et j'en extrait le fichier content.xml
    zip = new UnZipIt(monChemin, this);
    zip.unpackFile("content.xml", sketchPath("")+"/data/"+"content.xml");
  }
  unzipFini = true;
}

/*
×××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××
 On récupère le fichier content.xml qui contient la grande majorité 
 du texte du document selectionné
 */
void analyseXML() {
  //  --- je parcours le contenu de content.xml
  //  --- afin de récupérer les contenus textuels des balises text:p
  content = loadXML("content.xml");
  XML niveau_un = content.getChild("office:body");
  XML niveau_deux = niveau_un.getChild("office:text");
  XML [] children = niveau_deux.getChildren("text:p");

  String texte = "";
  for (int i = 0; i < children.length; i++) {
    int id = children[i].getInt("id");
    texte += children[i].getContent();
  }

  texte = texte.replaceAll("_", "");
  String s = " ¬ª¬´‚Äì_-–().,;:?!\u2014\"";
  String[] words = splitTokens(texte, s);
  nbTotaldeMots = words.length;

  for (int i=0; i<words.length; i++) {
    String mot = words[i].toLowerCase();
    tousLesMots.add(mot);
  }
  analyseXMLFinie = true;
}

/*
×××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××
 On compare les mots contenus dans content.xml pour repérer les
 occurences
 */
void compareMots() {
  textAlign(LEFT, CENTER);
  dico = new HashMap();
  // on vérifie dans la liste 'touslesmots' si un mot est déjà présent dans le dico, si 
  // c'est le cas on comptabilise une occurence de plus
  for (int i=0; i<tousLesMots.size(); i++) {
    String s = (String) tousLesMots.get(i);

    if (dico.containsKey(s)) {
      Mot m = (Mot) dico.get(s);
      m.count();
    }
    else {
      Mot m = new Mot(s);
      dico.put(s, m);
    }
  }

  Iterator i = dico.values().iterator();
  // x and y will be used to locate each word
  float x = W/50;
  float y = H/7;

  while (i.hasNext ()) {
    Mot m = (Mot) i.next();
    // ***************************************************************************
    // Si le mot apparait plus de 100 fois et qu'il est composé de plus de 5 lettres
    if (m.count >= nbOccurences.val && m.s.length() >= tailleMots.val) {
      // The size is the count
      float taillePolice = 12;//constrain(m.count, 5, 50);
      textFont(f, taillePolice);
      String message = m.s+"   "+m.count;
      //fill(0);
      text(message, x, y);
      // Move along the x-axis
      y += 20;
      //x += textWidth(m.s + " ");
    }
    //****************************************************************************

    // If x gets to the end, move Y
    if (y > height) {
      x += 150;
      y = H/7;
      // If y gets to the end, we're done
      if (x > width) {
        break;
      }
    }
  }
}

