class Bouton {
  float x, y, w;
  String t;
  int temp = 0;
  boolean active = false;

  Bouton(float _x, float _y, float _w, String _t) {
    x = _x;
    y = _y;
    w = _w;
    t = _t;
  }

  void dessine() {
    stroke(255);
    fill(0);
    rect(x, y, w, w, 5);
    textAlign(LEFT, CENTER);
    text(t, x - (textWidth(t) + (w)), y);
  }

  void miseAjour() {
    if (auDessus()) {
      if (mousePressed && temp == 0) {
        active = true;
        temp++;
      }
      else if (temp >= 1) {
        active = false;
      }
    }
    else {
      temp = 0;
    }
  }

  boolean auDessus() {
    if (mouseX >= x - w/2 && mouseX <= x + w/2 && mouseY >= y - w/2 && mouseY <= y + w/2) {
      return true;
    } 
    else {
      return false;
    }
  }
}

